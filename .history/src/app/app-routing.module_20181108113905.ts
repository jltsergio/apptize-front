import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleComponent } from './vehicle/vehicle.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path: 'vehicle', component: VehicleComponent},
  {path: 'user', component: UserComponent},
  {path: '', component: UserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
