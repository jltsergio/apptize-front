import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){}

  let currentDate = new Date();
  dateMassage:string = new Date().toDateString();
  
  title = 'Meu Primeiro Projeto';
}
