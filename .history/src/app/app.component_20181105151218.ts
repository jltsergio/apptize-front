import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){}

  dateMassage:string = new Date().toLocaleDateString() + new Date().toLocaleTimeString();
  
  title = 'Meu Primeiro Projeto';
}
