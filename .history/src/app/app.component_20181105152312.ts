import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  dateMassage:string;

  constructor(){
    setInterval(() => {
      this.dateMassage = new Date().toLocaleDateString() + new Date().toLocaleTimeString();
    }, 1000);
  }
    
  title = 'Meu Primeiro Projeto';
}
