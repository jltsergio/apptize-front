import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userList: AngularFireList<any>;

  constructor(private firebase:AngularFireDatabase) { 
  }
  formUser = new FormGroup({
    $key:new FormControl(null),
    fullName: new FormControl('', [Validators.required, Validators.minLength(8)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    age: new FormControl(''),
    sex: new FormControl('', Validators.required),
  })
  insertUser(user){
    this.userList.push({
      fullName: user.fullName,
      email: user.email,
      age: user.age,
      sex: user.sex
    });
  }

  getUsers(){
    this.userList = this.firebase.list('users');
    return this.userList.snapshotChanges();
  }

  populateForm(user){
    this.formUser.setValue(user);  
  }

  updateUser(user){
    this.userList.update(user.$key, {
      fullName: user.fullName,
      email: user.email,
      age: user.age,
      sex: user.sex
    })
  }

  deleteUser($key: string){
      this.userList.remove($key);
    }
}
