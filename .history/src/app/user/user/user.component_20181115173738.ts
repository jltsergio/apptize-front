import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


   formUser = this.userService.formUser.controls;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit(){

    this.submitted = true;

    if(this.userService.formUser.valid){
      this.userService.insertUser(this.userService.formUser.value);          
      this.showSucessMessage=true;
      setTimeout(() => this.showSucessMessage = false, 3000);
      this.submitted = false;
      this.userService.formUser.reset();     
}
  }
}

}

