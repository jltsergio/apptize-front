import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleComponent } from './vehicle/vehicle/vehicle.component';
import { ProdutoComponent } from './user/produto/produto.component';

const routes: Routes = [
  {path: 'vehicle', component: VehicleComponent},
  {path: 'produto', component: ProdutoComponent},
  {path: '', component: ProdutoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
''