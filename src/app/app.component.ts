import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dateMassage:string;
  
  constructor(private http: HttpClient){
    setInterval(() => {this.dateMassage = new Date().toLocaleDateString() + new Date().toLocaleTimeString()}, 1000)
    let obs = this.http.get('https://api.github.com/users/koushikkothagal');
    obs.subscribe((response) => console.log(response));
  }
}
