import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { environment } from '../environments/environment'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProdutoComponent } from './user/produto/produto.component';
import { VehicleComponent } from './vehicle/vehicle/vehicle.component';
import { FooterComponent } from './structure/footer/footer.component';
import { ProdutoService } from './user/produto/produto.service';
import { ProdutoListComponent } from './user/produto-list/produto-list.component';
import { SidebarComponent } from './structure/sidebar/sidebar.component';
import { VehicleListComponent } from './vehicle/vehicle-list/vehicle-list.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { OrderOfServiceComponent } from './order-of-service/order-of-service/order-of-service.component';

@NgModule({
  declarations: [
    AppComponent,
    ProdutoComponent,
    FooterComponent,
    ProdutoListComponent,
    VehicleComponent,
    //HeaderComponent,
    SidebarComponent,
    VehicleListComponent,
    OrderOfServiceComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas : [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
