import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'orderofservice',
  templateUrl: './order-of-service.component.html',
  styleUrls: ['./order-of-service.component.css']
})
export class OrderOfServiceComponent implements OnInit {

  formOrderOfService : FormGroup;

  constructor(private formBuilder : FormBuilder) {

  }


  ngOnInit() {
    this.formOrderOfService = this.formBuilder.group({
      client: null,
      employee: null,
      value: null
    });
  }

  onSave(){
    console.log(this.formOrderOfService.value);
  }

}
