import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../produto/produto.service'

@Component({
  selector: 'app-produto-list',
  templateUrl: './produto-list.component.html',
  styleUrls: ['./produto-list.component.css']
})
export class ProdutoListComponent implements OnInit {

  produtoArray = [];
  showDeletedMessage: boolean;
  searchText: string = "";

  constructor(private produtoService: ProdutoService) {
    
  }

  ngOnInit() {
    this.produtoService.getProdutos().then(x => {
      this.produtoArray = x
    });
  }

  onDelete(id: string){
    if(confirm('Tem certeza que deseja excluir esse registro ? ')){
      this.produtoService.deleteProduto(id);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }

  filterCondition(produto){
    this.produtoArray.forEach(x => {
      if(x.nome.toLowerCase().indexOf(produto.toLowerCase()) > -1){
        this.produtoArray.splice(x)
      }})
    //return produto.nome.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1;
  }
}


