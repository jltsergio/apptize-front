import { Component, OnInit } from '@angular/core';
import { ProdutoService } from './produto.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {


   formProduto = this.produtoService.formProduto.controls;

  constructor(private produtoService: ProdutoService) { 
    
  }

  submitted:boolean = false;
  showSucessMessage:boolean = false;

  ngOnInit() {
    let ss = '';
  }

  onSubmit(){
    console.log('teste')
    this.submitted = true;

    //if(this.produtoService.formProduto.valid){
      this.produtoService.insertProduto(this.produtoService.formProduto.value); 
      this.showSucessMessage=true;
      setTimeout(() => this.showSucessMessage = false, 3000);
      this.submitted = false;
      this.produtoService.formProduto.reset();     
//}

  }
}



