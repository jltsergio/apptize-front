import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  produtoList: any = [];

  constructor(private http: HttpClient) { 
  }
  

  
  formProduto = new FormGroup({
    id:new FormControl(null),
    nome: new FormControl('', [Validators.required, Validators.minLength(8)]),
    preco: new FormControl('', [Validators.required, Validators.email]),
    imagem: new FormControl(''),
    restaurante: new FormControl('', Validators.required),
    ativo: new FormControl(''),
  })


  insertProduto(produto){
    
    let _httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Headers':
          'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
      })
    };
  

    console.log(this.http)
    this.http.post('http://localhost:8080/produto', produto, _httpOptions)
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
    this.produtoList.push({
      nome: produto.nome,
      preco: produto.preco,
      imagem: produto.imagem,
      restaurant: produto.restaurante,
      ativo: produto.ativo,
    });
    window.location.reload()
  }

  async getProdutos(){
    console.log('teste')
    return await this.http.get('http://localhost:8080/produto').toPromise();

    //obs.subscribe((response) => this.produtoList = JSON.parse(JSON.stringify(response)));
    console.log(this.produtoList)
    //this.produtoList = JSON.parse(JSON.stringify(obs));
    return this.produtoList;
  }

  populateForm(produto){
    this.formProduto.setValue(produto);  
  }

  updateProduto(produto){
//    this.produtoList.update(produto.id, {
//      fullnome: produto.fullnome,
//      email: produto.email,
//      age: produto.age,
//      sex: produto.sex
//    })
  }

  deleteProduto(produto){
    this.http.delete('localhost:8080/produto', produto.id);
    }
}
