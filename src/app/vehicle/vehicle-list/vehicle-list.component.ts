import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../vehicle/vehicle.service'

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  vehicleArray = [];
  showDeletedMessage: boolean;
  searchText: string = "";

  constructor(private vehicleService: VehicleService) {
    
  }

  ngOnInit() {
    this.vehicleService.getVehicles().subscribe(
      list => {
      this.vehicleArray = list.map(item =>{
         return {
           $key: item.key,
        ...item.payload.val()
        };
      });
    });
  }

  onDelete($key: string){
    if(confirm('Tem certeza que deseja excluir esse registro ? ')){
      this.vehicleService.deleteVehicle($key);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }

  filterCondition(vehicle){
    return vehicle.board.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1;
  }

}
