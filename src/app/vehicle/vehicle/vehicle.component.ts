import { Component, OnInit } from '@angular/core';
import { VehicleService } from './vehicle.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  submitted: boolean;
  showSucessMessage: boolean;
  formUser = this.vehicleService.formVehicle.controls;

  constructor(private vehicleService: VehicleService) { }

  ngOnInit() {
  }

  onSubmit(){
      this.submitted = true;

      if(this.vehicleService.formVehicle.valid){
        if(this.vehicleService.formVehicle.get('$key').value == null){
          this.vehicleService.insertVehicle(this.vehicleService.formVehicle.value);
        }
        else{
          this.vehicleService.updateVehicle(this.vehicleService.formVehicle.value);
        }          
        this.showSucessMessage=true;
        setTimeout(() => this.showSucessMessage = false, 3000);
        this.submitted = false;
        this.vehicleService.formVehicle.reset();     
  }
}
}
