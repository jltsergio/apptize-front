import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  vehicleList: AngularFireList<any>;

  constructor(private firebase:AngularFireDatabase) { 
  }
  formVehicle = new FormGroup({
    $key:new FormControl(null),
    board: new FormControl('', [Validators.required, Validators.minLength(7)]),
    model: new FormControl('', [Validators.required]),
    color: new FormControl('')
  })
  insertVehicle(vehicle){
    this.vehicleList.push({
      board: vehicle.board,
      model: vehicle.model,
      color: vehicle.color
    });
  }

  getVehicles(){
    this.vehicleList = this.firebase.list('vehicles');
    return this.vehicleList.snapshotChanges();
  }

  populateForm(vehicle){
    this.formVehicle.setValue(vehicle);  
  }

  updateVehicle(vehicle){
    this.vehicleList.update(vehicle.$key, {
      board: vehicle.board,
      model: vehicle.model,
      color: vehicle.color
    })
  }

  deleteVehicle($key: string){
      this.vehicleList.remove($key);
    }
}
